# -*- coding: utf-8 -*-
FLASKCODE_APP_TITLE = 'FlaskCode'
FLASKCODE_EDITOR_THEME = 'vs-dark'
FLASKCODE_RESOURCE_BASEPATH = None

FLASKCODE_CAN_SAVE = True
"""Enable saving"""

FLASKCODE_CAN_RELOAD = True

FLASKCODE_ENABLE_PATH = False
"""Load file based on URL path"""

FLASKCODE_ENABLE_CREATE = False
"""Create files if they dont exist"""

FLASKCODE_REQUESTING_STRING = 'Requesting...'

FLASKCODE_NAVBAR_MENU = [
    dict(title='Home',url='/',icon='fa-chevron-left'),
]