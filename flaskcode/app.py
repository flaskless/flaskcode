from .cli import create_flask_app
import os

app = create_flask_app('admin','password')
app.config['FLASKCODE_RESOURCE_BASEPATH'] = os.getcwd()
app.config['FLASKCODE_ENABLE_PATH'] = True
app.config['FLASKCODE_ENABLE_CREATE'] = True

application = app.wsgi_app