# -*- coding: utf-8 -*-
import os
import mimetypes
from pathlib import Path
from flask import render_template, abort, jsonify, send_file, g, request, current_app
from .utils import write_file, dir_tree, get_file_extension
from . import blueprint

@blueprint.route('/',defaults={'path':None})
@blueprint.route('/<path:path>')
def index(path=None):
    dirname = os.path.basename(g.flaskcode_resource_basepath)
    file_path = os.path.basename(g.flaskcode_resource_file_path) if hasattr(g,'flaskcode_resource_file_path') and g.flaskcode_resource_file_path is not None else None
    dtree = dir_tree(g.flaskcode_resource_basepath, g.flaskcode_resource_basepath + '/')

    if current_app.config.get('FLASKCODE_ENABLE_PATH',False):
        file_path = file_path or path
    else:
        if path:
            abort(404)

    if file_path and not os.path.exists(file_path) and current_app.config.get('FLASKCODE_ENABLE_CREATE',False):
        Path(file_path).touch(mode=0o666, exist_ok=True)


    return render_template('flaskcode/index.html', dirname=dirname, dtree=dtree,file_path=file_path)
 

@blueprint.route('/resource-data/<path:file_path>.txt', methods=['GET', 'HEAD'])
def resource_data(file_path):
    file_path = os.path.join(g.flaskcode_resource_basepath, file_path)
    if not (os.path.exists(file_path) and os.path.isfile(file_path)):
        abort(404)
    response = send_file(file_path, mimetype='text/plain')
    mimetype, encoding = mimetypes.guess_type(file_path, False)
    if mimetype:
        response.headers.set('X-File-Mimetype', mimetype)
        extension = mimetypes.guess_extension(mimetype, False) or get_file_extension(file_path)
        if extension:
            response.headers.set('X-File-Extension', extension.lower().lstrip('.'))
    if encoding:
        response.headers.set('X-File-Encoding', encoding)
    return response


@blueprint.route('/update-resource-data/<path:file_path>', methods=['POST'])
def update_resource_data(file_path):
    file_path = os.path.join(g.flaskcode_resource_basepath, file_path)
    is_new_resource = bool(int(request.form.get('is_new_resource', 0)))
    if not is_new_resource and not (os.path.exists(file_path) and os.path.isfile(file_path)):
        abort(404)
    success = True
    message = 'File saved successfully'
    resource_data = request.form.get('resource_data', None)
    if resource_data:
        success, message = write_file(resource_data, file_path)
    else:
        success = False
        message = 'File data not uploaded'
    return jsonify({'success': success, 'message': message})
